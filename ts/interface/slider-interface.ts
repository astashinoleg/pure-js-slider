//interface for our Class Slider
interface ISlider {
    delay: number,
    width: number,
    height: number,
    slides: {
        color: string,
        text: string
    }[],
}

export default ISlider