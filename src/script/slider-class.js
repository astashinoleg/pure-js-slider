const createElem = require('./createElem.js')
// import createElem from "./createElem.js";
// this is where we'll put our slider
const renderDiv = document.getElementById("slider");
// variables for slider movement
let setPosition = 0;
let counterSlide = 0;
module.exports = counterSlide; 
// сreate a slider class
class Slider {
    constructor(options) {
        this.delay = options.delay,
            this.width = options.width,
            this.height = options.height,
            this.slides = options.slides;
    }
    // function that draws our slider
    render() {
        // create parent div that will contain our slides
        // let sliderDiv = document.createElement("div");
        const sliderDiv = createElem('div', 'js-slider');
        sliderDiv.style.cssText = `width: ${this.width}px; height: ${this.height}px`;
        // sliderDiv.classList.add('js-slider')
        // slider track for moving our slides
        const sliderTrack = createElem('div', 'js-slider__slideTrack');
        // let sliderTrack = document.createElement("div")
        // sliderTrack.classList.add("js-slider__slideTrack")
        // creating our slides
        for (let i = 0; i < this.slides.length; i++) {
            // create the slide itself
            const slide = createElem('div', 'js-slider__slide');
            // let slide = document.createElement("div")
            // slide.classList.add("js-slider__slide")
            slide.style.backgroundColor = `${this.slides[i].color}`;
            // create slide text
            const slideText = createElem('h1');
            // let slideText = document.createElement("h1")
            slideText.innerHTML = `${this.slides[i].text}`;
            // render our slides into the parent div        
            slide.appendChild(slideText);
            sliderTrack.appendChild(slide);
        }
        sliderDiv.appendChild(sliderTrack);
        // render the end result
        renderDiv.appendChild(sliderDiv);
    }
    // the function responsible for scrolling of slides
    swipe() {
        let trackDiv = document.querySelector(".js-slider__slideTrack");
        setPosition += 750;
        trackDiv.style.left = '-' + setPosition + 'px';
        counterSlide++;
    }
}

module.exports = Slider