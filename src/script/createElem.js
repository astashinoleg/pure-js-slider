function createElement(elemType, elemClass) {
    let elem = document.createElement(elemType);
    if (elemClass) {
        elem.classList.add(elemClass);
    }
    return elem;
}
module.exports = createElement
